/**
   This file is part of node-docker-registry.

   node-docker-registry is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   node-docker-registry is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with node-docker-registry.  If not, see <http://www.gnu.org/licenses/>.
*/

module.exports = function(server, config, log) {

    var db = require('../db')(config, log);

    server.get({url: /\/v2(\/?)$/}, function _v2_(req, res, next) {
        res.writeHead(200, {'X-Docker-Registry-Version': '2'});
        res.write('V2 Registry (node-docker-registry v' + config.version);
        res.end();
    });

    // https://index.docker.io/v2/library/busybox/tags/list
    // {"name":"library/busybox","tags":["1-ubuntu","1.21-ubuntu","1.21.0-ubuntu","1.23.2","1.23","1.24.0","1.24.1","1.24","1","buildroot-2013.08.1","buildroot-2014.02","latest","ubuntu-12.04","ubuntu-14.04","ubuntu"]}
    server.get('/v2/:repo/:name/tags/list', function _v2_get_manifests(req, res, next) {
        var name = req.params.name;
        var repo = req.params.repo;
        db.manifest.find({name: repo + '/' + name}).toArray(function(err, manifests) {
            if(err)
                return res.json(400, {errors: [{code: 'INTERNAL_ERROR',
                                                message: 'database error',
                                                detail: err}]});
            res.json(200, {name: repo + '/' + name, tags: manifests.map(function(m) { return m.tag; })});
            res.end();
        });
    });


    server.get('/v2/_catalog', function _v2_get_catalog(req, res, next) {
        var num = req.query.n;
        var query = {};
        if(!/^[0-9]+/.test(num) && num && num.length > 0) {
            query = {repository: num};
            num = false; // disables paging when querying for repo, but necessary..
        }
        db.manifest.distinct("name", query, function(err, repositories) {
            if(err)
                return res.json(500, {errors: [{code: 'INTERNAL_ERROR',
                                                message: 'Could not find repositories',
                                                detail: err.message}]});
            repositories = repositories.sort();
            var last = req.query.last;
            if(last) {
                var found = false;
                repositories = repositories.filter(function(r) { if(r === last) { found=true; return false;} return found;});
            }
            if(num) {
                if(repositories.length > num) {
                    last = repositories[ num-1 ];

                    var url = req.url.replace(/^([^\?]+).*$/, '$1');
                    res.setHeader("Link", "<" + url + "?n=" + num + "&last=" + last + ">; ref=next");
                }
                repositories = repositories.slice(0, Math.min(num, repositories.length));
            }
            return res.json(200, {repositories: repositories});
        });
    });
};
