/**
   This file is part of node-docker-registry.

   node-docker-registry is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   node-docker-registry is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with node-docker-registry.  If not, see <http://www.gnu.org/licenses/>.
*/
module.exports = function(server, config, log) {

    var fs = require('fs');
    var path = require('path');
    var db = require('../db')(config, log);
    var hash = require('../hash');
    var Pend = require('pend');
    var restify = require('restify');
    var jwk = require('jws-jwk');
    var crypto = require('crypto');

    // Create a new unique filename
    function get_new_file() {
        var id = hash.createSecureToken('blobpart');
        var file = path.join(config.storage.local.path, id[0], id[1], id[2], id);
        if(fs.existsSync(file)) { // collissions unlikely
            id = hash.createSecureToken('blobpart');
            file = path.join(config.storage.local.path, id[0], id[1], id[2], id);
        }

        try {
            var d1 = path.join(config.storage.local.path, id[0]);
            var d2 = path.join(config.storage.local.path, id[0], id[1]);
            var d3 = path.join(config.storage.local.path, id[0], id[1], id[2]);
            if(!fs.existsSync(d1))
                fs.mkdirSync(d1);
            if(!fs.existsSync(d2))
                fs.mkdirSync(d2);
            if(!fs.existsSync(d3))
                fs.mkdirSync(d3);
        } catch(e) {
            log.warn("Failed to create storage directory", e);
        }
        return file;
    }

    function verify_manifest(manifest, cb) {
        var pend = new Pend();
        manifest.fsLayers.filter(function(fsLayer) {
            pend.go(function(fsLayer) {
                return function(done) {
                    db.blob.get_by_digest(fsLayer.blobSum, function(err, blob) {
                        if(!blob)
                            return done({message:"blob unknown to registry", code: 'BLOB_UNKNOWN'});
                        done();
                    });
                };
            
            }(fsLayer));

        });

        pend.wait(function(err) {
            if(err)
                return cb(err);

            var valid_signatures = manifest.signatures.filter(function(sig) {
                // FIXME: Cannot find a proper EC module, and jws-jwk does not support EC
                return true; //jwk.verify(sig.signature, sig.header.jwk);
            });
            if(valid_signatures.length !== manifest.signatures.length) {
                return cb({code: 'SIGNATURE_MISMATCH',
                           message: 'One ore more signatures do not match'});
            }
            
            cb();
        });
    }

    function store_manifest(repository, man, cb) {
        db.manifest.get_by_repository_name_tag(repository,
                                               man.name.substr((man.name.indexOf('/')+1)),
                                               man.tag, function(err, exman) {
            if(err)
                return cb(err);
            if(exman) {
                db.manifest.update({id: exman.id},
                                   {$set: {manifest: man}
                                   }, function(err, resp) {
                                       if(err)
                                           return cb(err);
                                       log.info("Updated existing manifest", [resp.result]);
                                       cb(null);
                                   });
            } else {
                var new_manifest = {
                    repository: repository,
                    name: man.name.substr((man.name.indexOf('/')+1)),
                    tag: man.tag,
                    manifest: man
                };
                db.manifest.insert(new_manifest, function(err, new_item) {
                    if(err) 
                        return cb(err);
                    log.info("Added new manifest", [new_item]);
                    
                    cb(null);
                });
            }
        });
    }

    // put manifest
    server.put('/v2/:repo/:name/manifests/:tag',
               function(req, res, next) {

                   var manifest = "";
                   req.on('data', function(chunk) {
                       manifest += chunk.toString();
                   });
                   
                   req.on('end', function() {
                       try {
                           var man = JSON.parse(manifest);
                           verify_manifest(man, function(err) {
                               if(err)
                                   return res.json(400, {errors: [err]});
                               store_manifest(req.params.repo, man, function(err) {
                                   if(err) {
                                       err = {code: 'INTERNAL_ERROR',
                                              message: 'could not store manifest in database',
                                              detail: err};
                                       log.error('error when storing manifest', err);
                                       return res.json(500, {errors: [err]});
                                   }
                                   res.json(200, 'OK');
                               });
                           });
                       } catch(e) {
                           log.error("Error while storing manifest", e.message);
                           res.json(500, {error: [{code: 'INTERNAL_ERROR',
                                                   message: 'Cannot store manifest',
                                                   detail: e.message}]});
                           res.end();
                       }

                   });
               });

    
    // upload status
    server.get('/v2/:repo/:name/blobs/uploads/:uuid', function _get_blobs_uploads_uuid(req, res, next) {
        db.blob.get_by_id(req.params.uuid, function(err, blob) {
            if(err)
                return next(err);

            if(!blob)
                return next(new Error("No such blob1: " + req.params.uuid));

            res.setHeader('Range', 'bytes=0-' + blob.length);
            res.setHeader('Location: /v2/' + blob.repository + '/blobs/uploads/' + blob.id);
            res.setHeader('Docker-Upload-UUID', blob.id);
            res.send(200, '');
            res.end();
        });
    });

    // final chunk
    server.put('/v2/:repo/:name/blobs/uploads/:uuid', function _put_blobs_upload_uuid(req, res, next) {
        // The docker daemon 1.9.1 always PUTs a empty last chunk (even tho it does not have to)
        if(parseInt(req.headers['content-length'], 10) !== 0)
            return next(new Error("ImplLimit: Can only accept 0 Content-Length for final chunk"));

        db.blob.get_by_id(req.params.uuid, function(err, blob) {
            if(err)
                return next(err);

            if(!blob) {
                return next(new Error("No such blob2:" + req.params.uuid));
            }
            if(req.query.digest !== blob.digest) {
                return res.json(400, {errors: [{code: 'DIGEST_INVALID',
                                                message: 'Digest mismatch',
                                                detail: 'The server calculated a different digest than the one provided in the request'}]});
            }

            db.blob.update({id: blob.id},
                           {$set: {status: 'verified'}},
                           function(err) {
                               if(err) {
                                   log.error("Failed to update blob", err);
                                   return next(err);
                               }
                               
                               res.writeHead(201, {// Created
                                   'Docker-Content-Digest': digest,
                                   'Location': req.url,
                                   'Content-Length': '0'});
                               return res.end();
                           });
        });
    });

    // upload a chunk
    //    /v2/origo/busybox/blobs/uploads/b56271bc0ccf4533ede607d8e7313632d52d9b19c416c80637ba5fc92232c549
    server.patch('/v2/:repo/:name/blobs/uploads/:uuid', function _patch_blobs_upload_uuid(req, res, next) {
        db.blob.get_by_id(req.params.uuid, function(err, blob) {
            if(err)
                return next(err);

            if(!blob) {
                return next(new Error("No such blob2:" + req.params.uuid));
            }
            res.setHeader('Docker-Upload-UUID', blob.id);

            if(!req.isChunked()) {
                res.send(400, 'Bad request');
                return res.send("Must provide chunked request body");
            }

            var fileExists = fs.existsSync(blob.file);
            var offset = 0;
            var mode = 'w+';

            if(req.headers['content-range']) {
                offset = req.headers['content-range'].split(/-/,2)[0];
                mode = 'a+';
                log.trace('appending to file', [blob.file, offset, mode]);
            } else {
                log.trace('creating new file', [blob.file, offset, mode]);
            }

            // I have not found another way to modify highWaterMark, please advise
            // docker seems to send 32k chunks, but let's be future proof
            req.client._readableState.highWaterMark = 128 * 1024;

            fs.open(blob.file, mode, function(err, fd) {
                if(err)
                    return next(new Error('Could not create temporary file ' + blob.file, err));

                // Use pend, so we can write async
                var pend = new Pend();
                pend.max = 1;

                var file_shasum = crypto.createHash('sha256');
                
                // Update db with progress every 2s
                var progressTimer = setInterval(function() {
                    db.blob.update({id: blob.id, status: 'upload'}, {$set: {length: offset}});
                }, 2000);
                
                req.on('data', function(chunk) {
                    pend.go(function _pend_writer(pendDone) {
                        file_shasum.update(chunk);
                        fs.write(fd, chunk, 0, chunk.length, offset, function(err, written) {
                            if(err)
                                return pendDone(err);
                            if(written !== chunk.length)
                                return pendDone('Failed to write full chunk to disk, only wrote ' + written + ' out of ' + chunk.length);
                            offset += written;
                            pendDone();
                        });
                    });
                });
                
                req.on('end', function() {
                    log.trace('request ending. got total', offset);
                    pend.wait(function(err) {
                        clearInterval(progressTimer);
                        if(err) {
                            log.error('failed to write to disk', err);
                            return res.json(500, {errors: [{code: 'INTERNAL_ERROR',
                                                            message: 'Failed to write to disk',
                                                            detail: err}]});
                        }
                        var d = file_shasum.digest('hex');
                        var digest = 'sha256:' + d;
                        db.blob.update({id: blob.id,
                                        status: 'upload'},
                                       {$set: {length: offset,
                                               status: 'uploaded',
                                               digest: digest}});

                        res.writeHead('204', { // No Content
                            'Location': req.url,
                            'Range': '0-'+offset, // See https://github.com/docker/distribution/issues/1240
                            'Docker-Upload-UUID': req.params.uuid,
                            'Docker-Content-Digest': digest,
                            'Content-Length': '0'});
                        return res.end();
                    });
                });
                
            });
                
        });
    });
    
    // initiate new upload
    server.post('/v2/:repo/:name/blobs/uploads/:uuid', function(req, res, next) {
        if(!req.params.uuid) {
            var blob_id = hash.createSecureToken('blob');
            var new_blob = {digest: false,
                            repository: req.params.repo,
                            name: req.params.name,
                            file: get_new_file(),
                            length: 0,
                            status: 'upload',
                            id: blob_id};
            log.info("creating new blob", new_blob);
            db.blob.insert(new_blob, function(err, resp) {
                if(err) {
                    log.error("failed to create new blob", new_blob, err);
                    return next(err);
                } else {
                    log.info("created new blob", new_blob, err);
                    res.setHeader('Location', '/v2/' + new_blob.repository + '/' + new_blob.name + '/blobs/uploads/' + new_blob.id);
                    res.setHeader('Docker-Upload-UUID', new_blob.id);
                    res.setHeader('Range', 'bytes=0-0');
                    res.send(202, 'Go ahead, sir');
                }
                return next();
            });
        } else {
            // TODO: Handle POST upload
            db.blob.get_by_id(req.params.uuid, function(err, blob) {
                if(err)
                    return next(err);

                if(!blob)
                    return next(new Error("No such blob2:" + req.params.uuid));
                res.setHeader('Docker-Upload-UUID', blob.id);
                
                // TODO: Accept data
                
                return next(new Error("Not yet implemented"));
            });
        }
    });
    
};
