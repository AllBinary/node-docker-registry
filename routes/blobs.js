/**
   This file is part of node-docker-registry.

   node-docker-registry is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   node-docker-registry is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with node-docker-registry.  If not, see <http://www.gnu.org/licenses/>.
*/
module.exports = function(server, config, log) {

    var db = require('../db')(config, log);
    var fs = require('fs');

    // HEAD/info for blob
    server.head('/v2/:repo/:name/blobs/:digest', function(req, res, next) {
        db.blob.get_by_digest(req.params.digest, function(err, item) {
            if(err) {
                log.warn(req.url, err);
                res.json(404, 'Not Found');
                return next();
            }
            if(!item) {
                res.json(404, 'Not Found');
                return next();
            }
            res.setHeader('Content-Length', "" + (item.size|0) );
            res.setHeader('Docker-Content-Digest', item.digest);
            res.json(200, '');
            return next();
        });
    });

    server.get('/v2/:repo/:name/blobs/:digest', function(req, res, next) {
        db.blob.get_by_digest(req.params.digest, function(err, item) {
            if(err) {
                log.warn(req.url, err);
                res.json(404, 'Not Found');
                return next();
            }
            if(!item) {
                res.json(404, 'Not Found');
                return next();
            }

            item.size = fs.statSync(item.file).size;
            res.setHeader('Content-Length', "" + (item.size) );
            res.setHeader('Docker-Content-Digest', item.digest);
            log.info('sending file', [item.file, item.size]);
            var CHUNK_SIZE = 128 * 1024;
            var rs = fs.createReadStream(item.file, {highWaterMark: CHUNK_SIZE});
            rs.on('readable', function() {
                var chunk;
                while (null !== (chunk = rs.read(CHUNK_SIZE))) {
//                    log.info('writing ', chunk.length);
                    res.write(chunk);
                }
            });

            rs.on('end', function() {
                
                res.end();
            });
        });
        
    });
};
