/**
   This file is part of node-docker-registry.

   node-docker-registry is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   node-docker-registry is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with node-docker-registry.  If not, see <http://www.gnu.org/licenses/>.
*/
module.exports = function(server, config, log) {

    var db = require('../db')(config, log);
    
    server.get('/v2/:repo/:name/manifests/:tag', function _get_manifests(req, res, next) {
        var repo = req.params.repo;
        var name = req.params.name;
        var tag = req.params.tag;
        db.manifest.get_by_repository_name_tag(repo, name, tag, function(err, manifest) {
            if(err)
                return res.json(500, err);
            if(!manifest)
                return res.json(404, {errors: [{code: 'NOT_FOUND',
                                                message: 'No such manifest'}]});
            res.setHeader('Content-Type', 'application/json; charset=utf-8');
            res.setHeader('X-Docker-Registry-Version', '2');
            res.json(200, manifest.manifest);
            return res.end();
        });
    });


    return this;
};
