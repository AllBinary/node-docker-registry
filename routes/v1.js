/**
   This file is part of node-docker-registry.

   node-docker-registry is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   node-docker-registry is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with node-docker-registry.  If not, see <http://www.gnu.org/licenses/>.
*/

module.exports = function(server, config, log) {

    var db = require('../db')(config, log);

    server.get({url: '/v1/_ping'}, function _v1_ping(req, res, next) {
        res.writeHead(200, {'X-Docker-Registry-Version': '2'});
        return res.end();
    });

    server.get('/v1/search', function(req, res, next) {
        db.manifest.find({"name" : {$regex : req.query.q || ".*"}})
            .toArray(function(err, manifests) {
                res.json(200, {num_results: manifests.length,
                               query: req.query.q,
                               results: manifests.map(function(m) {
                                   return {name: m.name, description: m.manifest.description};
                               })
                              });
            });
    });
    
    server.get('/v1/repositories/:repo/:name/images', function _v1_repostories_images(req, res, next) {
        var repo = req.params.repo;
        var name = req.params.name;

        db.manifest.find({repository: repo,
                          name: name}).toArray(function(err, manifests) {
            if(err)
                return res.json(400, err);
            var digests = [];
            manifests.filter(function(m) {
                m.manifest.fsLayers.filter(function _fslayer(fsLayer) {
                    digests.push(fsLayer.blobSum);
                });
            });

            db.blob.find({digest: {$in: digests}}).toArray(function _find_blobs(err, blobs) {
                if(err) {
                    log.error(req.url, err);
                    return res.json(400, {code: 'INTERNAL_ERROR',
                                          message: 'Internal database error',
                                          detail: err});
                }
                res.json(200, blobs.map(function(b) { return {id: b.id,
                                                              checksum: 'tarsum+'+b.digest}; }));
            });
        });
    });

    server.get('/v1/repositories/:repo/:name/tags', function _get_manifests(req, res, next) {
        var name = req.params.name;
        var repo = req.params.repo;
        db.manifest.find({repository: repo,
                          name: name}).toArray(function(err, manifests) {
            if(err)
                return res.json(400, {code: 'INTERNAL_ERROR',
                                      message: 'database error',
                                      detail: err});
            var tags = {};
            manifests.map(function(m) {
                tags[m.manifest.tag] = m.id;
            });
        
            res.json(200, tags);
            res.end();
        });
    });

    
    // [{"pk": 21341729, "id": "17583c7d"}, {"pk": 21341730, "id": "d1592a71"}]

    server.get('/v1/repositories/:repo/:name/tags/:tag', function _v1_repostories_tags_tag(req, res, next) {
        var repo = req.params.repo;
        var name = req.params.name;
        var tag = req.params.tag;
        
        db.manifest.find({repository: repo,
                          name: name,
                          tag: tag}).toArray(function(err, manifests) {
                              if(err)
                                  return res.json(400, err);
                              var digests = [];
                              manifests.filter(function(m) {
                                  m.manifest.fsLayers.filter(function _fslayer(fsLayer) {
                                      digests.push(fsLayer.blobSum);
                                  });
                              });
                              
                              db.blob.find({digest: {$in: digests}}).toArray(function _find_blobs(err, blobs) {
                                  var i=1;
                                  res.json(200, blobs.map(function(b) { return {pk: i++,
                                                                                id: b.id};}));
                              });
                          });
    });

};
