#!/bin/sh

mkdir -p ssl/self-signed && cd ssl/self-signed

openssl req -new -x509 -extensions v3_ca -keyout ca.key -out ca.crt -days 1825

openssl genrsa -des3 -out server.key 1024
openssl rsa -in server.key -out server.key.insecure

mv server.key server.key.secure && mv server.key.insecure server.key
openssl req -new -key server.key -out server.csr
openssl x509 -CAcreateserial -CA ca.crt -CAkey ca.key -req -days 365 -in server.csr -signkey server.key -out server.crt
