/**
   This file is part of node-docker-registry.

   node-docker-registry is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   node-docker-registry is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with node-docker-registry.  If not, see <http://www.gnu.org/licenses/>.
*/
var fs = require('fs');
module.exports = {
    version: JSON.parse(fs.readFileSync('./package.json')).version,

    db: {
        mongo: {
            url: process.env.MONGO_URL || "mongodb://mongo:27017/registry"
        }
    },

    storage: {
        local: {
            path: process.env.STORAGE_PATH || '/tmp/'
        }
        // TODO: S3 Storage?
    },

    auth: {
        // If enabled, a JWT token is expected as Bearer auth. Only shared secret tokens are supported.
        // No validation except signature is done on the token (i.e repository access etc)
        enabled: process.env.AUTH_ENABLED || false,
        jwt_secret: process.env.JWT_SECRET || false,
        realm: process.env.AUTH_REALM || false,
        service: process.env.AUTH_SERVICE || false
    },
    
    server: {
        port: process.env.REGISTRY_PORT || 443,
        http_port: process.env.REGISTRY_HTTP_PORT || 80,
        name: 'node-docker-registry',
        version: JSON.parse(fs.readFileSync('./package.json')).version,
        timeout: process.env.SERVER_TIMEOUT || 60000,
        certificate: process.env.SSL_CERTIFICATE ? fs.readFileSync(process.env.SSL_CERTIFICATE).toString() : null,
        key: process.env.SSL_KEY ? fs.readFileSync(process.env.SSL_KEY).toString() : null,
        ca: process.env.SSL_CA ? fs.readFileSync(process.env.SSL_CA).toString() : null
    },

    log: {
        name: 'node-docker-registry',
        level: process.env.LOG_LEVEL || 'info'
    }
};

