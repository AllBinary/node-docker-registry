FROM phusion/baseimage:latest

RUN curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
RUN sudo apt-get install -y nodejs
RUN npm install -g bunyan

# Clean up APT
RUN apt-get autoremove -yq

# Enable ourselves as a service
RUN mkdir /etc/service/node-docker-registry
RUN echo "#!/bin/bash" >/etc/service/node-docker-registry/run
RUN echo "cd /node-docker-registry; npm start" >>/etc/service/node-docker-registry/run
RUN chmod +x /etc/service/node-docker-registry/run

# Processing dependencies from package.json is separate step, as it does not change often
ADD package.json /tmp/package.json
RUN npm config set loglevel info
RUN cd /tmp && npm install --production
RUN mkdir /node-docker-registry; cp -a /tmp/node_modules /node-docker-registry/

# Clean up
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Add main package files to /node-docker-registry/
ADD . /node-docker-registry/

# Make Dockerfile more visible
RUN mv /node-docker-registry/Dockerfile /

# Make sure services start
CMD ["/sbin/my_init"]


