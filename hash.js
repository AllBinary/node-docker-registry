/**
   This file is part of node-docker-registry.

   node-docker-registry is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   node-docker-registry is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with node-docker-registry.  If not, see <http://www.gnu.org/licenses/>.
*/

var crypto = require('crypto');
var fs = require('fs');

function makeHash(value, cb) {
    var shasum = crypto.createHash('sha256');
    shasum.update(value);
    var digest = shasum.digest('hex');
    if(cb)
	return cb(digest);
    return digest;
}

module.exports.digestFromFile = function(filename, cb) {
    var shasum = crypto.createHash('sha256');

    var CHUNK_SIZE = 256*1024;
    var s = fs.createReadStream(filename, {highWatermark: CHUNK_SIZE});
    s.on('readable', function () {
        var chunk;
        while (null !== (chunk = s.read(CHUNK_SIZE))) {
            shasum.update(chunk);
        }
    });

    s.on('end', function() {
        var d = shasum.digest('hex');
        cb(null, 'sha256:' + d);
    });
};

// Creates a sha256 hex digest token from 256 random bytes
module.exports.createSecureToken = function(prefix, cb) {
    var randomBytes = crypto.randomBytes(256);
    randomBytes = (prefix?prefix:"") + (randomBytes); //allows "namespacing" for hashes
    return makeHash(randomBytes, cb);
};

