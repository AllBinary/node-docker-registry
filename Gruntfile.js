/**
   This file is part of node-docker-registry.

   node-docker-registry is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   node-docker-registry is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with node-docker-registry.  If not, see <http://www.gnu.org/licenses/>.
*/
module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        vars: {
            deps: (Object.keys(grunt.file.readJSON('package.json').dependencies).join("/**,")+"/**").split(",")
        },
        bump: {
            options: {
                updateConfigs: ['pkg'],
                pushTo: 'origin',
                gitDescribeOptions: '--tags --always',
            }
        },
        copy: {
            build: {
                files: [
                    {expand: true, src: ['<%= pkg.files %>'], dest: 'build/', filter: 'isFile'}
                ]
            }
        },
        docker_io: {
            hub: {
                options: {
                    dockerFileLocation: 'build',
                    buildName: 'build/<%= pkg.name %>',
                    tag: ['<%= pkg.version %>'],
                    pushLocation: 'mani.allbin.se',
                    username: "user1",
                    password: "user1",
                    push: false,
                    force: false
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-bump');
    grunt.loadNpmTasks('grunt-docker-io');

    grunt.registerTask('docker', ['docker_io']);
    grunt.registerTask('default', ['copy:build', 'docker']);
};
