# node-docker-registry

### NodeJS Docker V2 Registry

This limited implementation of the Docker V2 Registry uses MongoDB, local file storage, and can
use a JWT token provider for basic Bearer auth. You can turn auth off, but the docker daemon
might behave strangely..

This implementation is far from production ready, but functions at a basic level. Use at your own risk,
and please do contribute if you want to (testing and issues are also contributions!)


### Features

- ##### Pros
- HTTPS and HTTP V1 & V2 repositories that work with Docker 1.9.1
- Local file storage with hashed directories for blobs
- Full push and pull support
- Token authentication support that mimics docker.io (but uses JWT)
- MongoDB storage for manifests and blob metadata
- Unlimited repositories / images / blobs
- Horizontally scalable
- Runs in a Docker container
- Works with --disable-legacy-registry=true to stop using Registry V1 protocol (deprecated)

- ##### Possible cons
- Minimal, limited authentication and access restriction support (disable built-in auth with AUTH_ENABLED=false and use a frontend proxy that do auth)
- Not fully (or much) tested
- No file locking. Your blobs might get corrupted. You've been warned.
- Does not clean up unreferenced blobs (yet).

### Requirements

- NodeJS v4.3 +
- MongoDB (see Quickstart for example how to run one)
- Optional: A JWT token provider that can use shared secret (for example https://hub.docker.com/r/allbinarian/jwt-token-provider/) or AUTH_ENABLED=false

### Quickstart

     # git clone https://bitbucket.org/AllBinary/node-docker-registry.git
     # npm install
     # grunt
     # docker build -t node-docker-registry:latest .
     # ./gencert.sh
     # mkdir storage
     # docker-compose -p registry -f config.yml up
     OR
     # docker run -d --name mongo_registry mongo:latest
     # sudo docker run -p 443:443 -p 80:80 \
                     -v $(pwd)/ssl:/ssl \
                     -v $(pwd)/storage:/storage \
                     -e LOG_LEVEL=info \
                     -e STORAGE_PATH=/storage \
                     -e REGISTRY_PORT=443 \
                     -e SSL_CA=/ssl/self-signed/ca.crt \
                     -e SSL_CERTIFICATE=/ssl/self-signed/server.crt \
                     -e SSL_KEY=/ssl/self-signed/server.key \
                     -e JWT_SECRET=jwtsecret \
                     -e AUTH_ENABLED=false \
                     --link mongo_registry:mongo \
                     -d \
                     node-docker-registry:latest
     Install the CA cert for Docker
     # sudo mkdir /etc/certs.d/your.host.tld
     # sudo cp /ssl/self-signed/ca.crt /etc/certs.d/your.host.tld/ca.crt
     # docker login your.host.tld

The Quickstart config.yml contains docker-registry-frontend:v2, so after pushing your first image, you
can point your browser to port 8080 to see that it actually got stored!

### Configuration
Configuration is done using environment variables, as per Docker standards:

    LOG_LEVEL=info [trace|warn|error]
    REGISTRY_PORT=443
    REGISTRY_HTTP_PORT=80
    SERVER_TIMEOUT=60000
    SSL_CA=null [/ssl/self-signed/ca.crt]
    SSL_CERTIFICATE=null [/ssl/self-signed/server.crt]
    SSL_KEY=null [/ssl/self-signed/server.key]
    # If AUTH_ENABLED, JWT_SECRET and AUTH_SERVICE/AUTH_REALM must also be set
    AUTH_ENABLED=false [true]
    JWT_SECRET=someveryhardtoguessjwtsecretsharedwiththeauthrealmservice
    AUTH_SERVICE=registry
    AUTH_REALM=http://localhost:50000/jwt_token


### What is this
This is a 'barebone' nodejs implementation of the Docker V2 HTTP API with local file storage.

### Why?
Because there seems to be only one implementation, in Go(Lang), and I had a weekend to spare.

### How?
By reading the docs, analyzing HTTP traffic, trial & error, plus some luck, I managed to figure out
the basic protocol for push/pull, auth and signatures, for the current (1.9.1) version of docker.

### What is missing
Well, alot. The current code base is **NOT in a production state**. Specifically, it lacks most of any
reasonable security and auth assertions. It does however work for my daily use, and once I get around
to it, I might implement more features. If you'd like to contribute, please fork away, and finally,
use at your own risk.

### License

GPL/3