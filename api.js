/**
   This file is part of node-docker-registry.

   node-docker-registry is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   node-docker-registry is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with node-docker-registry.  If not, see <http://www.gnu.org/licenses/>.
*/
var config = require('./config');
var bunyan = require('bunyan');
var restify = require('restify');

var log = config.server.log = bunyan.createLogger(config.log);

log.info("config", config);

if(config.server.certificate) {
    var server = restify.createServer(config.server);
    setupServer(server);

    server.listen(config.server.port, function(err) {
        if(err) {
            log.error("Failed to start listening on port " + config.server.port, err);
            process.exit(2);
        }
        log.info("%s listening at %s", server.name, config.server.port);
    });
}

var server2 = restify.createServer({name: config.server.name + '-http',
                                    version: config.server.version,
                                    timeout: config.server.timeout});
setupServer(server2);

server2.listen(config.server.http_port, function(err) {
    if(err) {
        log.error("Failed to start listening on port " + config.server.http_port, err);
        process.exit(2);
    }
    log.info("%s listening at %s", server2.name, config.server.http_port);
});

function setupServer(server) {

    server.use(restify.acceptParser(server.acceptable));
    server.use(restify.authorizationParser());
    server.use(restify.dateParser());
    server.use(restify.queryParser({mapParams: false}));
    server.use(restify.fullResponse());         // sets up all of the default headers for the system

    server.pre(function _server_pre(req, res, next) {
        log.info("Request", {method: req.method,
                             url: req.url,
                             headers: req.headers,
                             query: req.getQuery(),
                             params: req.params,
                             body: req.body
                            });
        return next();
    });

    server.use(function _docker_dist_api_version(req, res, next) {
        res.setHeader('Docker-Distribution-API-Version', 'registry/2.0');
        next();
    });

    require('./routes/auth.js')(server, config, log);
    require('./routes/v2.js')(server, config, log);
    require('./routes/v1.js')(server, config, log);
    require('./routes/manifests.js')(server, config, log);
    require('./routes/blobs.js')(server, config, log);
    require('./routes/uploads.js')(server, config, log);

    server.on('uncaughtException', function _uncaught_exception(req, res, obj, err) {
        log.error('uncaughtException', obj, err);
    });

    server.on('after', function _server_after(req, res, err, next) {
        if(res.statusCode === 404)
            log.info("404 Encountered", {url:req.url,
                                         headers: req.headers,
                                         query: req.getQuery(),
                                         params: req.params,
                                         body: req.body});
        if(!err)
            return;
    });

}

